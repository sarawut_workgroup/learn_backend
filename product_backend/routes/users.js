const express = require('express')
const router = express.Router()
const User = require('../models/User')

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function (req, res, next) {
  const id = req.params.id

  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addUser = async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    res.status(500).send({
      message: err.message
    })
  }
}

const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

router.get('/', getUsers)// get all users
router.get('/:id', getUser)// get one user by id
router.post('/', addUser)// add user
router.put('/:id', updateUser)// edit user
router.delete('/:id', deleteUser)// delete user

module.exports = router
