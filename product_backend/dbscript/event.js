const mongoose = require('mongoose')
const Event = require('../models/Event')
mongoose.connect('mongodb://localhost:27017/example')
async function clearEvent () {
  await Event.deleteMany({})
}
async function main () {
  await clearEvent()
  await Event.insertMany([
    {
      title: 'TItle 1', content: 'Content 1', startDate: new Date('2022-03-03 08:00'), endDate: new Date('2022-03-03 16:00'), class: 'video_time'
    },
    {
      title: 'TItle 2', content: 'Content 2', startDate: new Date('2022-03-01 08:00'), endDate: new Date('2022-03-30 16:00'), class: 'working'
    },
    {
      title: 'TItle 3', content: 'Content 3', startDate: new Date('2022-03-15 12:00'), endDate: new Date('2022-03-25 20:00'), class: 'a'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('Finish')
})
