const { ROLE } = require('../constant')
const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const { Schema } = mongoose
const userSchema = Schema({
  username: String,
  password: String, // plain text
  roles: {
    type: [String],
    default: [ROLE.USER]
  }
})

userSchema.pre('save', function (next) {
  const user = this

  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (saltError, salt) {
      if (saltError) {
        return next(saltError)
      } else {
        bcrypt.hash(user.password, salt, function (hashError, hash) {
          if (hashError) {
            return next(hashError)
          }

          user.password = hash
          next()
        })
      }
    })
  } else {
    return next()
  }
})

module.exports = mongoose.model('User', userSchema)
